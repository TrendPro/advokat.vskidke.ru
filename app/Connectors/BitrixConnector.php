<?php

namespace App\Connectors;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Cookie;

class BitrixConnector {

    protected $requestData = '';
	protected $visits = [];


    protected function getVisitParam($param)
    {
		$value = null;
		if (!is_array($this->visits)) {
			return null;
		}
        foreach ($this->visits as $visit)
        {
            if (!empty($visit[$param]))
            {
                $value = $visit[$param];
            }
        }
        return $value;
    }

    protected function openConnection ($url, $arFields)
    {
        $curl = \curl_init($url);

        $data['params'] = array("REGISTER_SONET_EVENT" => "Y");
        $data['fields'] = $arFields;


        \curl_setopt_array($curl, [
            CURLOPT_RETURNTRANSFER  =>  true,
            CURLOPT_POST => 1,
            CURLOPT_POSTFIELDS => http_build_query($data)
        ]);

        return $curl;
    }

    public function addLead ($data)
    {
		$this->visits = $data['visits'] ?? [];
        info($this->visits);

		// info($this->visits);
        $data = [
            'TITLE' =>  $data['title'],
            'SOURCE_ID' => "WEB", //Источник вебсайт
            'PHONE_MOBILE' => str_replace(['+7', ' ', '-', '(', ')'], ['8', ''], $data['phone'] ?? ''),
            'UTM_SOURCE' => request()->get('utm_source') ?? $this->getVisitParam('utm_source'),
            'UTM_MEDIUM' => request()->get('utm_medium') ?? $this->getVisitParam('utm_medium'),
            'UTM_CAMPAIGN' => request()->get('utm_campaign') ?? $this->getVisitParam('utm_campaign'),
            'UTM_CONTENT' => request()->get('utm_content') ?? $this->getVisitParam('utm_content'),
            'UTM_TERM' => request()->get('utm_term') ?? $this->getVisitParam('utm_term'),
            'PHONE' => Array(
                "n0" => Array(
                    "VALUE" => str_replace(['+7', ' ', '-', '(', ')'], ['8', ''], $data['phone'] ?? ''),
                    "VALUE_TYPE" => "MOBILE",
                ),
            ),
        ];

        $connection = $this->openConnection('https://mrgeorge.bitrix24.ru/rest/20/mw9sm8i775rohf7q/crm.lead.add.json', $data);

        $result = \curl_exec($connection);

		return $result;
    }

    public function addTask ()
    {

    }
}
