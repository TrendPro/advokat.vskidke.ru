<?php

namespace App\Http\Controllers;

use App\Connectors\BitrixConnector;
use Illuminate\Http\Request;
use App\Lead;
use Illuminate\Support\Facades\Mail;
use App\Mail\LeadSendMail;

class  LeadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $leads = Lead::All()->reverse();

        $data = [
            'title' => 'Заявки',
            'leads' => $leads,
        ];

        return view('admin.lead.index', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $arVisits = session('visits');

        $input = $request->except('_token');
        $lead = Lead::create($input);

        //Mail::to('sos@7482361.ru')->send(new LeadSendMail($lead));

        $bitrixConnector = new BitrixConnector;

        $res = $bitrixConnector->addLead([
            'visits' => $arVisits,
            'title'	=>	'Земельный Юрист-ЛИД МАГНИТ',
            'phone'	=> $request->phone,
        ]);
        //Mail::to('popsy2007@mail.ru')->send(new LeadSendMail($lead));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Lead $lead)
    {

        $data = [
            'title' => 'Заявка',
            'lead' => $lead
        ];

        return view('admin.lead.show',$data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Lead $lead)
    {
        $lead->delete();
        return redirect()->route('leads.index')->with('status','Заявка удалена');
    }
}
