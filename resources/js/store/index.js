import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        siteUrl: 'https://xn------qddacabkhgcee3aegeghihta0cex2a4dzl3a2e.xn--p1ai/',
        isModal: false,
        isSuccess: false,
    }
})
